## Initialization
```sh
docker-compose up -d
docker exec -it golang_git_server /bin/bash
```

## Install dependencies
```sh
go get
```

## Run server
```sh
go run main.go
```